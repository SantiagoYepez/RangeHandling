use lib '.';
use strict;
use warnings;
use YERangeHandling::Chain;

### Create a blessed `HASH` as `Person` called `$malcolm`.
my $malcolm = bless {
    name => 'Malcolm Young'
}, 'Person';

### Create a `Node` object that encloses `$malcolm`
### within the range from 1953 to 2017
my $malcolmNode = Node->new($malcolm, Range->new(1953, 2017));

### Print the value of the `name` key of the
### data structure stored by $malcolmNode.
print "Name: " . $malcolmNode->value->{name} . "\n";