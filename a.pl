use lib '.';
use strict;
use warnings;
use YERangeHandling::TextGrid;

my $rnd = sub { return int rand 10000; };

my $dummyRef = sub {
	my ($val) = @_;
	return sub { return $val; };
};

my $iteratorGetter = sub {
	my ($from, $to) = @_;
	return sub {
		my $start = $from; # $rnd->();
		my $end = $from + 1; # $start + $rnd->();
		return $from <= $to ? Node->new($dummyRef->($from++), Range->new($start, $end)) : undef;
	};
};

my $it = $iteratorGetter->(1, 10000);
my @nodes;

print "Creating...\n";
while (defined (my $n = $it->())) {
	push @nodes, $n;
}

print "Adding...\n";
my $chain = Chain->new("test")->insert(@nodes);
#my $empty = Chain->new("empty")->insert($chain->fillAndIterateWithinRange('VOID'));

print "End\n";
#print $chain->toString . "\n";
#print $empty->toString . "\n";
